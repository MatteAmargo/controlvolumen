int valor = 0;    //Valor del potenciometro
int valor_m = 0;  //Valor del potenciometro mapeado a 0-100
int vol_new = 0;  //Volumen nuevo
int vol_old = 0;  //Volumen viejo

void setup() {
  pinMode (A1, INPUT);        //Se establece el pin Analogico 1 como entrada
  pinMode(3, INPUT_PULLUP);   //Se establece el pin Digital como entrada 'PULLUP'
  pinMode(13, OUTPUT);        //Se establece el pin 13 como salida (LED incorporado)
  Serial.begin(9600);         //Inicializacion del puerto serie

}

void loop() {
  valor = analogRead(A1);
  valor_m = map(valor, 0, 1023, 0, 101);  //Mapeo de valores de 0 a 100
  vol_new = int(valor_m / 5) * 5;         //hace que amento|disminucion del volumen sea de 5 en 5
  if (abs(vol_new - vol_old)>0){
    vol_old = vol_new;
    Serial.println(vol_old);
    delay(10);
  }
  int mute = digitalRead(3);
   if (mute == HIGH) {
    digitalWrite(13, LOW);
  } else {
    digitalWrite(13, HIGH);
    Serial.println(int(-1));  //Señal para mute/unmute
    delay(500);               //Pequeño retraso para que solo detecte un click del boton
  }
}

