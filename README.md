# Control de Volumen
Controlador de volumen Arduino - Alsa (linux)
## Descripción
Controlador de volumen con Arduino y Python.
El script de Python se ejecuta en el ordenador para recibir las instrucciones del arduino a través del puerto serie.
En este proyecto se utilizó:

- GNU/Linux Debian 10
- Python 3.7
- Arduino nano (ATmega328)
- Arduino IDE 1.8.13
- 1 potenciometro
- 1 boton
- cables (varios) 

## Diagrama de conexion

<details><summary>Click para expandir/contraer</summary>

![Diagrama de conexion](./img/volumeControl.png)
</details>

## Resolución de problemas
Si al instalar pyalsaaudio (`pip pyalsaaudio`) se obtiene un error, verificar que la libreria `libasound2-dev` esté instalda.  
`apt install libasound2-dev`

## Referencias

- El proyecto del siguiente link fue modificado de acuerdo a mis necesidades y pasado a español.
    - https://microcontrollerelectronics.com/serial-port-communication-via-python/
    - En este link encontrarás comousar Python para leer los datos que llegan por el puerto serie.
