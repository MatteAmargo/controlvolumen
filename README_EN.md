# Volume control
Arduino volume controller - Alsa (linux)
## Description
Volume controller with Arduino and Python. The Python script runs on the computer to receive the instructions from the Arduino through the serial port. In this project we used:

- GNU / Linux Debian 10
- Python 3.7
- Arduino nano (ATmega328)
- Arduino IDE 1.8.13
- 1 potentiometer
- 1 button
- wires

## Connection diagram
<details><summary>Click to expand / collapse</summary>

![Connection diagram](./img/volumeControl.png)
</details>

## Problem resolution
If when installing pyalsaaudio (`pip pyalsaaudio`) you get an error, check that the `libasound2-dev` library is installed.  
`apt install libasound2-dev`

## References

- The project of the following link was modified according to my needs and translated into Spanish.
    - https://microcontrollerelectronics.com/serial-port-communication-via-python/
    - In this link you will find how to use Python to read the data that arrives through the serial port.