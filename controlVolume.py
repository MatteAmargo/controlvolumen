#!/bin/python3
# Proyecto Control de Volumen con arduino
# pip3 install pyserial, pyalsaaudio

import serial
import sys
import glob
import alsaaudio

### Detectando el puerto de conexión *
dev = "/dev/ttyACM*"
scan = glob.glob(dev)
rate = "9600"
if (len(scan) == 0):
    dev = '/dev/ttyUSB*'
    scan = glob.glob(dev)
    if (len(scan) == 0):
        print("Unable to find any ports scanning for /dev/[ttyACM*|ttyUSB*] - " + dev)
        sys.exit()

serport = scan[0]
#Descomentar las siguientes 3 lineas para ver en pantalla los puertos encontrados
# for i in range(0, len(scan)):
#     print(i, " - ", scan[i])
# print("Puerto a utilizar: " + serport)

### Conectando con el Arduino
ser = serial.Serial(port=serport,baudrate=rate,timeout=1)

mixer = alsaaudio.Mixer()
while True:
    try:
        line = int(ser.readline())  #Leer puerto serie
        # print(line)  #Imprimir en pantalla el valor leido
        if (-1 < line < 101):
            # Descomentar la siguiente linea para ver en pantalla el valor de volumen
            # print(line)
            mixer.setvolume(line)  # Set volumen global
        elif (line < 0):
            if (line == -1):
                mixer.setmute(abs(mixer.getmute()[0]) + line)  # Conmutar mute/unmute

    except KeyboardInterrupt:  #Sucede cuando se presiona 'control-d' o 'delete|suprimir'
        sys.exit()
    except:
        pass
